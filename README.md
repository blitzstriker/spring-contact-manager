### Connecting DigitalOcean droplet with SSH

> ssh -i <ssh_key_filename> root@<ip_address_droplet>

Example:
> ssh -i id_rsa root@159.65.152.155

### Change mysql root password

>  alter user 'root'@'localhost' identified with mysql_native_password by 'mysqladmin'

### Securing password with Jasypt

Add the below dependency and plugin:

```xml
<!--Under dependencies-->
<dependency>
    <groupId>com.github.ulisesbocchio</groupId>
    <artifactId>jasypt-spring-boot-starter</artifactId>
    <version>3.0.4</version>
</dependency>

<!--Under plugins-->
<plugin>
    <groupId>com.github.ulisesbocchio</groupId>
    <artifactId>jasypt-maven-plugin</artifactId>
    <version>3.0.4</version>
</plugin>
```

Encrypting a password:
> mvn jasypt:encrypt-value -Djasypt.encryptor.password=secretkey -Djasypt.plugin.value=password

Add the encrypted value in the properties file

Decrypting a password:
> mvn jasypt:decrypt-value -Djasypt.encryptor.password=secretkey -Djasypt.plugin.value=encryptedkey

To access the encrypted value at the runtime, add the below command in the VM Options in Run/Debug Configuration in IntelliJ
> -Djasypt.encryptor.password=secretkey