package com.blitzstriker.projectmanagement.controllers;

import com.blitzstriker.projectmanagement.dto.Message;
import com.blitzstriker.projectmanagement.entities.User;
import com.blitzstriker.projectmanagement.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.security.Principal;

@Controller
@RequestMapping("/settings")
@RequiredArgsConstructor
public class SettingsController {

    private final UserService userService;

    private final PasswordEncoder passwordEncoder;

    @GetMapping("/edit/password")
    public String showChangePasswordForm(Model model) {
        model.addAttribute("title", "Password - User Settings");
        return "main/change-password";
    }

    @PostMapping("/update-password")
    public String updatePassword(
            @RequestParam("current_password") String currentPassword,
            @RequestParam("new_password") String newPassword,
            @RequestParam("confirm_password") String confirmPassword,
            Principal principal,
            HttpSession httpSession,
            Model model
    ) {
        User user = userService.getUserByEmail(principal.getName());
        if (!passwordEncoder.matches(currentPassword, user.getPassword())) {
            httpSession.setAttribute("message", new Message("Your current password does not match", "alert-warning"));
            return "redirect:/settings/edit/password";
        }

        if (newPassword.length() < 8) {
            httpSession.setAttribute("message", new Message("Password is too short (must be at least 8 characters)", "alert-danger"));
            return "redirect:/settings/edit/password";
        }

        if (!newPassword.equals(confirmPassword)) {
            httpSession.setAttribute("message", new Message("Password confirmation does not match", "alert-warning"));
            return "redirect:/settings/edit/password";
        }

        userService.updatePassword(user, newPassword);
        httpSession.setAttribute("message", new Message("Your Password has been successfully updated", "alert-success"));
        return "redirect:/settings/edit/password";
    }
}
