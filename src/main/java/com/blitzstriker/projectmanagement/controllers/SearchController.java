package com.blitzstriker.projectmanagement.controllers;

import com.blitzstriker.projectmanagement.dto.ContactResponse;
import com.blitzstriker.projectmanagement.entities.User;
import com.blitzstriker.projectmanagement.services.ContactService;
import com.blitzstriker.projectmanagement.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/search")
@RequiredArgsConstructor
public class SearchController {

    private final ContactService contactService;

    private final UserService userService;

    @GetMapping("/{keyword}")
    public ResponseEntity<List<ContactResponse>> searchContact(@PathVariable("keyword") String keyword, Principal principal) {
        User user = userService.getUserByEmail(principal.getName());
        return new ResponseEntity<>(contactService.searchContactByUser(keyword, user), HttpStatus.OK);
    }
}
