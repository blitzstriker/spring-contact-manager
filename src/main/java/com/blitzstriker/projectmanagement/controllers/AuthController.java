package com.blitzstriker.projectmanagement.controllers;

import com.blitzstriker.projectmanagement.dto.Message;
import com.blitzstriker.projectmanagement.entities.User;
import com.blitzstriker.projectmanagement.services.EmailService;
import com.blitzstriker.projectmanagement.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.Random;

@Controller
@RequestMapping("/auth")
@RequiredArgsConstructor
public class AuthController {
    private final UserService userService;

    private final EmailService emailService;

    public Boolean isAuthenticated() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null || authentication instanceof AnonymousAuthenticationToken) {
            return false;
        }
        return true;
    }

    @GetMapping("/login")
    public String showCustomLogin(Model model) {
        model.addAttribute("title", "Login");
        if (isAuthenticated()) {
            return "redirect:/users/dashboard";
        }
        return "auth/login";
    }

    @GetMapping("/register")
    public String displayRegisterPage(Model model) {
        model.addAttribute("title", "Register");

        User user = new User();
        model.addAttribute("user", user);
        return "auth/register";
    }

    @PostMapping("/createUser")
    public String registerUser(@Valid @ModelAttribute("user") User user, BindingResult errors, Model model, HttpSession httpSession) {
        model.addAttribute("title", "Register");
        try {
            if (errors.hasErrors()) {
                System.out.println("Error Occurred!!");
                model.addAttribute("user", user);
                return "auth/register";
            }

            if (user.getPassword().length() < 8) {
                System.out.println("Password must be at least 8 characters.");
                throw new Exception("Password must be at least 8 characters.");
            }

            userService.createUser(user);
            model.addAttribute("user", new User());
            httpSession.setAttribute("message", new Message("Your account has been created successfully.", "alert-success"));
            return "redirect:/auth/register";
        } catch (Exception e) {
            e.printStackTrace();
            model.addAttribute("user", user);
            httpSession.setAttribute("message", new Message(e.getMessage(), "alert-danger"));
            return "auth/register";
        }
    }

    @GetMapping("/verify-email")
    public String showVerifyEmailForm(Model model) {
        model.addAttribute("title", "Verify Email");
        return "auth/verify-email";
    }

    @PostMapping("/verify-email")
    public String verifyAndSendEmail(@RequestParam("email") String email, HttpSession httpSession) {
        try {
            User user = userService.getUserByEmail(email);

            // Generate security code
            Random random = new Random();
            int randomCode = random.nextInt(999999);
            String securityCode = String.format("%06d", randomCode);

            String emailMessage = "You have requested to reset your password. Use this security code to change your password. " + securityCode;
            Boolean messageFlag = emailService.sendEmail("Reset Password", emailMessage, email);

            if(!messageFlag) {
                httpSession.setAttribute("message", new Message("Something went wrong. Please try again.", "alert-danger"));
                return "redirect:/auth/verify-email";
            }
            httpSession.setAttribute("code", securityCode);
            httpSession.setAttribute("verificationEmail", email);
            httpSession.setAttribute("message", new Message("Email verified. A verification code is sent to your email", "alert-success"));
            return "redirect:/auth/reset-password";
        } catch (Exception e) {
            e.printStackTrace();
            httpSession.setAttribute("message", new Message("Email is not registered", "alert-danger"));
            return "redirect:/auth/verify-email";
        }
    }

    @GetMapping("/reset-password")
    public String showResetPassword(Model model) {
        model.addAttribute("title", "Reset Password");
        return "auth/reset-password";
    }

    @PostMapping("/reset-password")
    public String verifyAndResetPassword(
            @RequestParam("verification_code") String verificationCode,
            @RequestParam("new_password") String newPassword,
            @RequestParam("confirm_password") String confirmPassword,
            HttpSession httpSession
    ) {
        String code = (String) httpSession.getAttribute("code");
        String email = (String) httpSession.getAttribute("verificationEmail");

        if(!code.equals(verificationCode)) {
            httpSession.setAttribute("message", new Message("Verification code is incorrect. Please try again.", "alert-danger"));
            return "redirect:/auth/reset-password";
        }

        User user = userService.getUserByEmail(email);

        if (newPassword.length() < 8) {
            httpSession.setAttribute("message", new Message("Password is too short (must be at least 8 characters)", "alert-danger"));
            return "redirect:/auth/reset-password";
        }

        if (!newPassword.equals(confirmPassword)) {
            httpSession.setAttribute("message", new Message("Password confirmation does not match", "alert-warning"));
            return "redirect:/auth/reset-password";
        }

        userService.updatePassword(user, newPassword);
        httpSession.setAttribute("message", new Message("Your password has been reset successfully", "alert-success"));
        return "redirect:/auth/login";
    }
}
