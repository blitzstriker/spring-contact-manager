package com.blitzstriker.projectmanagement.controllers;

import com.blitzstriker.projectmanagement.dto.Message;
import com.blitzstriker.projectmanagement.entities.Contact;
import com.blitzstriker.projectmanagement.entities.User;
import com.blitzstriker.projectmanagement.services.ContactService;
import com.blitzstriker.projectmanagement.services.FileService;
import com.blitzstriker.projectmanagement.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    private final ContactService contactService;

    private final FileService fileService;

    public void addCommonData(Model model, Principal principal) {
        String username = principal.getName();
        User user = userService.getUserByEmail(username);
        model.addAttribute("user", user);
    }

    public Boolean isAuthenticated() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null || authentication instanceof AnonymousAuthenticationToken) {
            return false;
        }
        return true;
    }

    @GetMapping("/dashboard")
    public String showDashboard(Model model) {
        model.addAttribute("title", "Dashboard");
        return "main/dashboard";
    }

    @GetMapping("/add-contact")
    public String showAddContactForm(Model model) {
        model.addAttribute("title", "Add Contact");
        Contact contact = new Contact();
        model.addAttribute("contact", contact);
        return "main/add-contact";
    }

    @PostMapping("/createContact")
    public String createContact(@Valid @ModelAttribute("contact") Contact contact, BindingResult errors, @RequestParam("profileImage") MultipartFile file, Model model, Principal principal, HttpSession httpSession) {
        try {
            if (errors.hasErrors()) {
                System.out.println("Error Occurred!!");
                model.addAttribute("contact", contact);
                model.addAttribute("title", "Add Contact");
                return "main/add-contact";
            }

            if (String.valueOf(contact.getPhone()).length() != 10) {
                System.out.println("Phone number must be 10 digits");
                errors.rejectValue("phone", "error.contact", "Phone number must be 10 digits");
                model.addAttribute("contact", contact);
                model.addAttribute("title", "Add Contact");
                return "main/add-contact";
            }

            String imageUrl = "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png";

            if (!file.isEmpty()) {
                imageUrl = fileService.uploadImage(file);
            }

            User user = userService.getUserByEmail(principal.getName());
            contact.setImageUrl(imageUrl);
            contactService.createContact(contact, user);
            httpSession.setAttribute("message", new Message("Contact successfully added.", "alert-success"));
            return "redirect:/users/add-contact";
        } catch (Exception e) {
            e.printStackTrace();
            model.addAttribute("contact", contact);
            httpSession.setAttribute("message", new Message(e.getMessage(), "alert-danger"));
            return "main/add-contact";
        }
    }

    @GetMapping("/all-contacts/{page}")
    public String showContacts(@PathVariable("page") Integer page, Model model, Principal principal) {
        model.addAttribute("title", "All Contacts");

        User user = userService.getUserByEmail(principal.getName());
        Long totalContacts = contactService.findContactsByUser(user);
        model.addAttribute("totalContacts", totalContacts);

        Page<Contact> contactsByPage = contactService.getAllContacts(user.getId(), page);
        List<Contact> allContacts = contactsByPage.getContent();
        model.addAttribute("contacts", allContacts);
        model.addAttribute("currentPage", page);
        model.addAttribute("lastPage", contactsByPage.getTotalPages());
        model.addAttribute("hasNext", contactsByPage.hasNext());
        model.addAttribute("hasPrevious", contactsByPage.hasPrevious());
        model.addAttribute("isFirst", contactsByPage.isFirst());
        model.addAttribute("isLast", contactsByPage.isLast());
        return "main/show-contacts";
    }

    @GetMapping("/contact/{id}")
    public String showContact(@PathVariable("id") Long id, Model model, HttpSession httpSession, Principal principal) {
        model.addAttribute("title", "View Contact");
        try {
            User user = userService.getUserByEmail(principal.getName());
            Contact contact = contactService.getContactById(id);

            if (user.getId() == contact.getUser().getId()) {
                model.addAttribute("contact", contact);
            } else {
                httpSession.setAttribute("message", new Message("Oops! No contact found", "alert-warning"));
            }
            return "main/show-contact";
        } catch (Exception e) {
            e.printStackTrace();
            httpSession.setAttribute("message", new Message(e.getMessage(), "alert-danger"));
            return "main/show-contact";
        }
    }

    @GetMapping("/delete-contact/{id}")
    public String deleteContact(@PathVariable("id") Long id, Model model, HttpSession httpSession, Principal principal) {
        try {
            User user = userService.getUserByEmail(principal.getName());
            Contact contact = contactService.getContactById(id);

            if (user.getId() == contact.getUser().getId()) {
                contactService.deleteContactById(id);
                httpSession.setAttribute("message", new Message("Contact deleted successfully", "alert-success"));
            } else {
                httpSession.setAttribute("message", new Message("Oops! No contact found", "alert-warning"));
            }
            return "redirect:/users/all-contacts/1";
        } catch (Exception e) {
            e.printStackTrace();
            httpSession.setAttribute("message", new Message(e.getMessage(), "alert-danger"));
            return "redirect:/users/all-contacts/1";
        }
    }

    @GetMapping("/edit-contact/{id}")
    public String showUpdateContact(@PathVariable("id") Long id, Model model, HttpSession httpSession, Principal principal) {
        model.addAttribute("title", "Update Contact");
        try {
            User user = userService.getUserByEmail(principal.getName());
            Contact contact = contactService.getContactById(id);

            if (user.getId() == contact.getUser().getId()) {
                model.addAttribute("contact", contact);
            } else {
                httpSession.setAttribute("message", new Message("Oops! No contact found", "alert-warning"));
            }
            return "main/update-contact";
        } catch (Exception e) {
            e.printStackTrace();
            httpSession.setAttribute("message", new Message(e.getMessage(), "alert-danger"));
            return "main/update-contact";
        }
    }

    @PostMapping("/updateContact")
    public String updateContact(@Valid @ModelAttribute("contact") Contact contact, BindingResult errors, @RequestParam("profileImage") MultipartFile file, HttpSession httpSession, Model model, Principal principal) {
        try {
            if (errors.hasErrors()) {
                System.out.println("Error Occurred!!");
                model.addAttribute("contact", contact);
                model.addAttribute("title", "Update Contact");
                return "main/update-contact";
            }

            if (String.valueOf(contact.getPhone()).length() != 10) {
                System.out.println("Phone number must be 10 digits");
                errors.rejectValue("phone", "error.contact", "Phone number must be 10 digits");
                model.addAttribute("contact", contact);
                model.addAttribute("title", "Update Contact");
                return "main/update-contact";
            }

            if(!file.isEmpty()) {
                String imageUrl = fileService.uploadImage(file);
                contact.setImageUrl(imageUrl);
            }

            contactService.updateContactById(contact, contact.getId());
            httpSession.setAttribute("message", new Message("Contact successfully updated.", "alert-success"));
            return "redirect:/users/contact/" + contact.getId();
        } catch (Exception e) {
            e.printStackTrace();
            model.addAttribute("contact", contact);
            httpSession.setAttribute("message", new Message(e.getMessage(), "alert-danger"));
            return "main/update-contact";
        }
    }
}
