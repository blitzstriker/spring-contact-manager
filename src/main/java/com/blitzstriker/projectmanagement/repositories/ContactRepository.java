package com.blitzstriker.projectmanagement.repositories;

import com.blitzstriker.projectmanagement.entities.Contact;
import com.blitzstriker.projectmanagement.entities.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContactRepository extends JpaRepository<Contact, Long> {
    Page<Contact> findByUserId(Long id, Pageable pageable);

    @Query("select count(c) from Contact c where c.user=:user")
    Long getCountByUser(@Param("user") User user);

    List<Contact> findByFirstNameContainingAndUser(String firstName, User user);
}
