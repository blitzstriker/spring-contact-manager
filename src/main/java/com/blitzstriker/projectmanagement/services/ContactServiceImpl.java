package com.blitzstriker.projectmanagement.services;

import com.blitzstriker.projectmanagement.dto.ContactResponse;
import com.blitzstriker.projectmanagement.entities.Contact;
import com.blitzstriker.projectmanagement.entities.User;
import com.blitzstriker.projectmanagement.repositories.ContactRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ContactServiceImpl implements ContactService{
    private final ContactRepository contactRepository;

    private final ModelMapper modelMapper;

    @Override
    public void createContact(Contact contact, User user) {
        contact.setUser(user);
        contactRepository.save(contact);
    }

    @Override
    public Page<Contact> getAllContacts(Long userId, Integer page) {
        System.out.println("Getting contacts for page: " + page);
        Pageable pageable = PageRequest.of(page - 1, 5);
        return contactRepository.findByUserId(userId, pageable);
    }

    @Override
    public Contact getContactById(Long id) {
        return contactRepository.findById(id).orElseThrow(() -> new RuntimeException("No Contact found"));
    }

    @Override
    public void deleteContactById(Long id) {
        Contact contact = contactRepository.findById(id).orElseThrow(() -> new RuntimeException("No Contact found"));
        contactRepository.delete(contact);
    }

    @Override
    public Long findContactsByUser(User user) {
        return contactRepository.getCountByUser(user);
    }

    @Override
    public void updateContactById(Contact contact, Long id) {
        Contact existingContact = contactRepository.findById(id).orElseThrow(() -> new RuntimeException("No Contact found"));
        existingContact.setFirstName(contact.getFirstName() != null ? contact.getFirstName() : existingContact.getFirstName());
        existingContact.setLastName(contact.getLastName() != null ? contact.getLastName() : existingContact.getLastName());
        existingContact.setEmail(contact.getEmail() != null ? contact.getEmail() : existingContact.getEmail());
        existingContact.setPhone(contact.getPhone() != null ? contact.getPhone() : existingContact.getPhone());
        existingContact.setDescription(contact.getDescription() != null ? contact.getDescription() : existingContact.getDescription());
        existingContact.setNickName(contact.getNickName() != null ? contact.getNickName() : existingContact.getNickName());
        existingContact.setImageUrl(contact.getImageUrl() != null ? contact.getImageUrl() : existingContact.getImageUrl());
        contactRepository.save(existingContact);
    }

    @Override
    public List<ContactResponse> searchContactByUser(String query, User user) {
        List<Contact> contacts = contactRepository.findByFirstNameContainingAndUser(query, user);
        return contacts.stream().map(contact -> modelMapper.map(contact, ContactResponse.class)).collect(Collectors.toList());
    }
}
