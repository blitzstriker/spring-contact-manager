package com.blitzstriker.projectmanagement.services;

public interface EmailService {
    Boolean sendEmail(String subject, String message, String to);
}
