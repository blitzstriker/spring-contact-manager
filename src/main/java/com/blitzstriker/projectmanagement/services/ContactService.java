package com.blitzstriker.projectmanagement.services;

import com.blitzstriker.projectmanagement.dto.ContactResponse;
import com.blitzstriker.projectmanagement.entities.Contact;
import com.blitzstriker.projectmanagement.entities.User;
import org.springframework.data.domain.Page;

import java.util.List;

public interface ContactService {
    void createContact(Contact contact, User user);

    Page<Contact> getAllContacts(Long userId, Integer page);

    Contact getContactById(Long id);

    void deleteContactById(Long id);

    Long findContactsByUser(User user);

    void updateContactById(Contact contact, Long id);

    List<ContactResponse> searchContactByUser(String query, User user);
}
