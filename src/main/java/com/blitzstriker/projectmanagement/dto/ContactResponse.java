package com.blitzstriker.projectmanagement.dto;

import lombok.Data;

@Data
public class ContactResponse {
    private Long id;
    private String firstName;
    private String lastName;
    private String imageUrl;
}
