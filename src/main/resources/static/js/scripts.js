const ctx = document.getElementById("myPieChart");

let decodedChartData = decodeHtml(chartData)
let jsonData = JSON.parse(decodedChartData);

let labelData = [];
let countData = [];

jsonData.forEach(data => labelData.push(data.label))
jsonData.forEach(data => countData.push(data.count))

console.log(labelData, countData)

const data = {
    labels: labelData,
    datasets: [{
        label: 'Project Status',
        data: countData,
        backgroundColor: [
            'rgb(255, 99, 132)',
            'rgb(54, 162, 235)',
            'rgb(255, 205, 86)'
        ],
        hoverOffset: 4
    }]
};

new Chart(ctx, {
    type: 'pie',
    data: data,
    options: {
        plugins: {
            title: {
                display: true,
                text: 'Project Status'
            }
        }
    }
})

function decodeHtml(html) {
    let txt = document.createElement("textarea");
    txt.innerHTML = html;
    return txt.value;
}

function showModal(id) {
    let deleteBtn = document.querySelector("#deleteBtn");
    let href = `/users/delete-contact/${id}`;
    deleteBtn.setAttribute("href", href);
}

async function search() {
    let searchInput = document.querySelector("#search");
    let searchResult = document.querySelector(".search-result");
    let query = searchInput.value;
    let url = `/api/search/${query}`;
    if(query.trim() === "") {
        searchResult.style.display = "none"
    } else {
        const response = await fetch(url);
        const data = await response.json();
        let searchList = `<div class="list-group list-group-flush">`
        data.forEach(contact => {
            searchList += `<a href="/users/contact/${contact.id}" class="list-group-item list-group-item-action d-flex align-items-center">
                                <img src="${contact.imageUrl}" class="rounded me-2" width="30">
                                <span>${contact.firstName} ${contact.lastName}</span></span>
                           </a>`
        });
        searchList += `</div>`;
        searchResult.innerHTML = searchList;
        searchResult.style.display = "block";
    }
}